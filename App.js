/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import firebase from '@react-native-firebase/app';

// import Appsrc from './src/screens/Tugas1/Tugas1';
// import Appsrc from './src/screens/Tugas2';
// import Appsrc from './src/screens/Tugas3/todoList';
// import Appsrc from './src/screens/Tugas4';
// import Appsrc from './src/screens/Tugas6/routes';
// import Appsrc from './src/screens/Tugas7/routes';
// import Appsrc from './src/screens/Tugas8/routes';
// import Appsrc from './src/screens/Tugas9/routes';
import Appsrc from './src/screens/Tugas10/routes';
// import Appsrc from './src/screens/Tugas10/screens/editProfilecopy';

var firebaseConfig = {
  apiKey: 'AIzaSyDFruwmwrPNJRGbcIep6OFTHxPaIpJUI4g',
  authDomain: 'deatrainingapp.firebaseapp.com',
  databaseURL: 'https://deatrainingapp.firebaseio.com',
  projectId: 'deatrainingapp',
  storageBucket: 'deatrainingapp.appspot.com',
  messagingSenderId: '686259295616',
  appId: '1:686259295616:web:4844ca542319f5b36f5d65',
  measurementId: 'G-ZL43Z0MHT3',
};
// Inisialisasi firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
// Initialize Firebase
// firebase.initializeApp(firebaseConfig);
// firebase.analytics();
const App: () => React$Node = () => {
  return (
    <>
      <Appsrc />
    </>
  );
};

export default App;
