import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default function Login() {
  return (
    <View style={styles.container}>
      <Text>Ini adalah halaman Login</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
