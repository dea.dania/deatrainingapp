import Intro from './intro';
import SplashScreens from './splash';
import Login from './login';
import Register from './register';

export {Intro, SplashScreens, Login, Register};
