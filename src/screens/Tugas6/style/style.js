import {StyleSheet, Dimensions} from 'react-native';
import colors from './colors';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  slider: {
    flex: 1,
  },
  btnContainer: {
    marginBottom: height * 0.03,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  btnLogin: {
    height: height * 0.05,
    width: width * 0.9,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.main,
  },
  btnTextLogin: {
    fontSize: width * 0.04,
    fontWeight: 'bold',
    color: colors.white,
  },
  btnRegister: {
    height: height * 0.05,
    width: width * 0.9,
    borderWidth: 1.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: colors.main,
    backgroundColor: 'transparent',
  },
  btnTextRegister: {
    fontSize: width * 0.04,
    fontWeight: 'bold',
    color: colors.main,
  },
  listContainer: {
    marginTop: height * 0.03,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  listContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgList: {
    width: width * 1.7,
    height: width * 1.1,
  },
  textList: {
    marginTop: height * 0.05,
    fontSize: width * 0.035,
    fontFamily: 'RobotoRegular',
    color: colors.black,
  },
  activeDotStyle: {
    width: width * 0.06,
    backgroundColor: colors.black,
  },
});

export default styles;
