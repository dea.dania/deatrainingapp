import Intro from './intro';
import SplashScreens from './splash';
import Login from './login';
import Register from './register';
import Profile from './profile';

export {Intro, SplashScreens, Login, Register, Profile};
