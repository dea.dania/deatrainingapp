import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Wallet from '../../assets/icons/wallet.svg';
import Settings from '../../assets/icons/settings.svg';
import Help from '../../assets/icons/help.svg';
import TermsCond from '../../assets/icons/terms.svg';
import Logout from '../../assets/icons/logout.svg';

const {width, height} = Dimensions.get('window');
const Tugas2 = () => {
  return (
    <>
      <StatusBar backgroundColor="#00BCD4" barStyle="light-content" />
      <View style={styles.container}>
        {/* Header */}
        <View style={styles.header}>
          <Text style={styles.headerText}>Account</Text>
        </View>
        {/* Profile : image and name */}
        <View style={styles.profile}>
          <Image
            source={require('../../assets/img/woman.png')}
            style={styles.img}
          />
          <Text style={styles.profileText}>Dea Dania</Text>
        </View>
        {/* Saldo */}
        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.subItemSaldo}>
              <Wallet width={50} height={50} />
              <View style={{marginLeft: -(width * 0.3)}}>
                <Text style={styles.itemText}>Saldo</Text>
              </View>
              <Text style={styles.itemText}>Rp. 999.999.999</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.subItem}>
              <Settings width={50} height={50} />
              <Text style={styles.itemText}>Pengaturan</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.subItem}>
              <Help width={50} height={50} />
              <Text style={styles.itemText}>Bantuan</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.subItem}>
              <TermsCond width={50} height={50} />
              <Text style={styles.itemText}>Syarat dan Ketentuan</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.subItem}>
              <Logout width={50} height={50} />
              <Text style={styles.itemText}>Keluar</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};
export default Tugas2;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
  },
  header: {
    backgroundColor: '#00BCD4',
  },
  headerText: {
    marginVertical: width * 0.05,
    marginHorizontal: width * 0.05,
    fontSize: 24,
    color: 'white',
  },
  profile: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: width * 0.05,
    paddingHorizontal: width * 0.05,
    backgroundColor: 'white',
    // justifyContent: 'space-around',
    // alignItems: 'flex-start',
  },
  img: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderRadius: 25,
  },
  profileText: {
    marginLeft: width * 0.05,
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: width * 0.01,
  },
  item: {
    marginVertical: width * 0.01,
  },
  subItemSaldo: {
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: width * 0.05,
    paddingVertical: width * 0.05,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subItem: {
    marginBottom: width * 0.01,
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: width * 0.05,
    paddingVertical: width * 0.05,
    alignItems: 'center',
  },
  itemText: {
    marginLeft: width * 0.06,
    fontSize: 14,
  },
});
