import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default function Tugas1() {
  return (
    <>
      <View style={styles.container}>
        <Text>Halo Kelas React Native Lanjutan Sanbercode!</Text>
      </View>
      <View></View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
