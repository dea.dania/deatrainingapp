import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Wallet from '../../../assets/icons/wallet.svg';
import Settings from '../../../assets/icons/settings.svg';
import Help from '../../../assets/icons/help.svg';
import TermsCond from '../../../assets/icons/terms.svg';
import Logout from '../../../assets/icons/logout.svg';

import api from '../../../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';

const {width, height} = Dimensions.get('window');
const Profile = ({navigation}) => {
  const [data, setData] = useState({});
  const [UserInfo, setUserInfo] = useState(null);
  const [isGoogle, setIsGoogle] = useState(false);
  console.log(UserInfo);
  console.log(isGoogle);
  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        // console.log('token => ', token);
        if (token !== null) return getProfile(token);
        // console.log(Profile);
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getToken();
    getProfile();
    getCurrentUser();
  }, []);

  const getCurrentUser = async () => {
    try {
      const usInfo = await GoogleSignin.signInSilently();
      console.log('usinfo=>', usInfo);
      setUserInfo(usInfo);
      setIsGoogle(true);
      // console.log(userInfo);
      // console.log(isGoogle);
      // console.log(userInfo.name);
      // console.log(userInfo.photo);
    } catch (error) {
      setIsGoogle(false);
    }
    // console.log('userinfo =', usInfo);
    // setUserInfo(usInfo.user);
    // console.log('state userinfo=>', userInfo);
    // setProfile(usInfo.user);
    // console.log('state profle =>', Profile);
    // setImgSrc(usInfo.user.photo);
    // console.log('state imgsrc=>', JSON.stringify(imgSrc));
  };

  const getProfile = (token) => {
    Axios.get(`${api.api}/profile/get-profile`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
        Accept: 'aplication/json',
        'Content-Type': 'aplication/json',
      },
    })
      .then((res) => {
        console.log('res = >', res);
        setData(res.data.data.profile);
      })
      .catch((err) => {
        console.log('error=>', err);
      });
  };

  const onLogoutPress = async () => {
    try {
      if (UserInfo !== null) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
      await AsyncStorage.removeItem('token');
      navigation.navigate('Login');
    } catch {
      (err) => {
        console.log('error =>', err);
      };
    }
  };

  return (
    <>
      <StatusBar backgroundColor="#00BCD4" barStyle="light-content" />
      <View style={styles.container}>
        {/* Header */}
        <View style={styles.header}>
          <Text style={styles.headerText}>Account</Text>
        </View>
        {/* Profile : image and name */}
        <View style={styles.profile}>
          <Image
            key={new Date().getTime()}
            source={
              isGoogle == false
                ? data.photo == null
                  ? require('../../../assets/img/woman.png')
                  : {
                      uri: api.home + data.photo + '?' + new Date(),
                      cache: 'reload',
                      headers: {Pragma: 'no-cache'},
                    }
                : {uri: UserInfo && UserInfo.user && UserInfo.user.photo}
            }
            style={styles.img}
          />
          <Text style={styles.profileText}>
            {isGoogle == false
              ? data.name
              : UserInfo && UserInfo.user && UserInfo.user.name}
          </Text>
        </View>
        {/* Saldo */}
        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.subItemSaldo}>
              <Wallet width={50} height={50} />
              <View style={{marginLeft: -(width * 0.3)}}>
                <Text style={styles.itemText}>Saldo</Text>
              </View>
              <Text style={styles.itemText}>Rp. 999.999.999</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.subItem}>
              <Settings width={50} height={50} />
              <Text style={styles.itemText}>Pengaturan</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.subItem}>
              <Help width={50} height={50} />
              <Text style={styles.itemText}>Bantuan</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.subItem}>
              <TermsCond width={50} height={50} />
              <Text style={styles.itemText}>Syarat dan Ketentuan</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.item}>
          <TouchableOpacity onPress={() => onLogoutPress()}>
            <View style={styles.subItem}>
              <Logout width={50} height={50} />
              <Text style={styles.itemText}>Keluar</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};
export default Profile;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
  },
  header: {
    backgroundColor: '#00BCD4',
  },
  headerText: {
    marginVertical: width * 0.05,
    marginHorizontal: width * 0.05,
    fontSize: 24,
    color: 'white',
  },
  profile: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: width * 0.05,
    paddingHorizontal: width * 0.05,
    backgroundColor: 'white',
    // justifyContent: 'space-around',
    // alignItems: 'flex-start',
  },
  img: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderRadius: 25,
  },
  profileText: {
    marginLeft: width * 0.05,
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: width * 0.01,
  },
  item: {
    marginVertical: width * 0.01,
  },
  subItemSaldo: {
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: width * 0.05,
    paddingVertical: width * 0.05,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subItem: {
    marginBottom: width * 0.01,
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: width * 0.05,
    paddingVertical: width * 0.05,
    alignItems: 'center',
  },
  itemText: {
    marginLeft: width * 0.06,
    fontSize: 14,
  },
});
