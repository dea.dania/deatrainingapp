import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  Button,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from 'react-native';
import colors from '../style/colors';
import api from '../../../api';
import Axios from 'axios';
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-community/async-storage';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';

export default function Login({navigation}) {
  const [email, setEmail] = useState('zakkymf@gmail.com');
  const [password, setPassword] = useState('123456');
  const mountedRef = useRef(true);
  const saveToken = async (token) => {
    try {
      // if (!mountedRef.current) return null;
      console.log('Login => res ', token);
      await AsyncStorage.setItem('token', token.data.data.token);
      const tok = await AsyncStorage.getItem('token');
      console.log('token => ', tok);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    configureGoogleSignIn();
    mountedRef.current = false;
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '686259295616-o20u2u67dqqbkiang2d8apj8ii2ba28t.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log('signInwithgoogle => idtoken', idToken);
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      navigation.navigate('Profile');
    } catch (error) {
      console.log('signin with google  => error', error);
    }
  };

  const onLoginPress = () => {
    let data = {
      email: email,
      password: password,
    };
    Axios.post(`${api.api}/auth/login`, data, {
      timeout: 20000,
    })
      .then((res) => {
        // console.log('Login => res ', res);
        saveToken(res);
        Keyboard.dismiss();
        navigation.navigate('Profile');
      })
      .catch((err) => {
        console.log('error => ', err);
        Alert.alert('OOPS', 'check your email and password again', [
          {
            text: 'OK',
            onPress: () => {
              Keyboard.dismiss();
              console.log('alert close');
            },
          },
        ]);
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Welcome back</Text>
        <View>
          <Text style={styles.subHeadText}>let's grow your business</Text>
        </View>
      </View>
      <View style={styles.form}>
        <TextInput
          value={email}
          placeholder="Email"
          onChangeText={(email) => setEmail(email)}
          style={styles.textInput}
        />
        <TextInput
          value={password}
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
          placeholder="Password"
          style={styles.textInput}
        />

        <View style={styles.buttonAlign}>
          <TouchableOpacity style={styles.theButton}>
            <Button
              title="LOGIN"
              color={colors.main}
              // onPress={() => navigation.navigate('Profile')}>
              onPress={() => onLoginPress()}></Button>
          </TouchableOpacity>
        </View>
        <View style={styles.buttonAlign}>
          <GoogleSigninButton
            onPress={() => signInWithGoogle()}
            style={{width: '100%', height: 40}}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
          />
        </View>
      </View>
      <View style={styles.subFootText}>
        <Text>don't have an account yet?</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
          <Text style={{marginLeft: 10, fontWeight: 'bold'}}>REGISTER</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    // backgroundColor: 'yellow',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
  },
  header: {
    marginTop: 40,
    paddingVertical: 10,
    paddingHorizontal: 30,
    // backgroundColor: 'blue',
    backgroundColor: '#ffffff',
    flexDirection: 'column',
  },
  subHeadText: {
    fontFamily: 'RobotoRegular',
    fontSize: 20,
    marginTop: 10,
  },
  subFootText: {
    flexDirection: 'row',
    fontFamily: 'ComfortaaBold',
    fontSize: 20,
    marginRight: 30,
    justifyContent: 'flex-end',
  },
  headerText: {
    fontFamily: 'RobotoBold',
    fontSize: 36,
  },
  form: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  formLabel: {
    paddingLeft: 20,
  },
  textInput: {
    margin: 15,
    padding: 5,
    height: 40,
    borderColor: 'black',
    borderWidth: 0.5,
    borderRadius: 2,
  },
  buttonAlign: {
    alignItems: 'stretch',
    marginHorizontal: 15,
  },
  theButton: {
    paddingVertical: 20,
  },
});
