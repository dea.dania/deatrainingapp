import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Wallet from '../../../assets/icons/wallet.svg';
import Settings from '../../../assets/icons/settings.svg';
import Help from '../../../assets/icons/help.svg';
import TermsCond from '../../../assets/icons/terms.svg';
import Logout from '../../../assets/icons/logout.svg';

import api from '../../../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const {width, height} = Dimensions.get('window');
const Profile = ({navigation}) => {
  const [Profile, setProfile] = useState({});
  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        // console.log('token => ', token);
        getProfile(token);
        // console.log(Profile);
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getToken();
  });

  const getProfile = (token) => {
    Axios.get(`${api}/profile/get-profile`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
      },
    })
      .then((res) => {
        setProfile(res.data.data.profile);
        // console.log(Profile);
      })
      .catch((err) => {
        console.log('error=>', err);
      });
  };
  const onLogoutPress = async () => {
    try {
      await AsyncStorage.removeItem('token');
      navigation.navigate('Login');
    } catch {
      (err) => {
        console.log('error =>', err);
      };
    }
  };

  return (
    <>
      <StatusBar backgroundColor="#00BCD4" barStyle="light-content" />
      <View style={styles.container}>
        {/* Header */}
        <View style={styles.header}>
          <Text style={styles.headerText}>Account</Text>
        </View>
        {/* Profile : image and name */}
        <View style={styles.profile}>
          <Image
            // source={require('../../../assets/img/woman.png')}
            source={{
              uri: 'https://crowdfunding.sanberdev.com' + Profile.photo,
            }}
            style={styles.img}
          />
          <Text style={styles.profileText}>{Profile.name}</Text>
        </View>
        {/* Saldo */}
        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.subItemSaldo}>
              <Wallet width={50} height={50} />
              <View style={{marginLeft: -(width * 0.3)}}>
                <Text style={styles.itemText}>Saldo</Text>
              </View>
              <Text style={styles.itemText}>Rp. 999.999.999</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.subItem}>
              <Settings width={50} height={50} />
              <Text style={styles.itemText}>Pengaturan</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.subItem}>
              <Help width={50} height={50} />
              <Text style={styles.itemText}>Bantuan</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.subItem}>
              <TermsCond width={50} height={50} />
              <Text style={styles.itemText}>Syarat dan Ketentuan</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.item}>
          <TouchableOpacity onPress={() => onLogoutPress()}>
            <View style={styles.subItem}>
              <Logout width={50} height={50} />
              <Text style={styles.itemText}>Keluar</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};
export default Profile;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
  },
  header: {
    backgroundColor: '#00BCD4',
  },
  headerText: {
    marginVertical: width * 0.05,
    marginHorizontal: width * 0.05,
    fontSize: 24,
    color: 'white',
  },
  profile: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: width * 0.05,
    paddingHorizontal: width * 0.05,
    backgroundColor: 'white',
    // justifyContent: 'space-around',
    // alignItems: 'flex-start',
  },
  img: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderRadius: 25,
  },
  profileText: {
    marginLeft: width * 0.05,
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: width * 0.01,
  },
  item: {
    marginVertical: width * 0.01,
  },
  subItemSaldo: {
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: width * 0.05,
    paddingVertical: width * 0.05,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subItem: {
    marginBottom: width * 0.01,
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: width * 0.05,
    paddingVertical: width * 0.05,
    alignItems: 'center',
  },
  itemText: {
    marginLeft: width * 0.06,
    fontSize: 14,
  },
});
