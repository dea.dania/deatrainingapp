import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  Button,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from 'react-native';
import colors from '../style/colors';
import api from '../../../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
export default function Login({navigation}) {
  const [email, setEmail] = useState('zakkymf@gmail.com');
  const [password, setPassword] = useState('123456');

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
      // const tok = await AsyncStorage.getItem('token');
      // console.log('token => ', tok);
    } catch (err) {
      console.log(err);
    }
  };

  const onLoginPress = () => {
    let data = {
      email: email,
      password: password,
    };
    Axios.post(`${api}/auth/login`, data, {
      timeout: 20000,
    })
      .then((res) => {
        // console.log('Login => res ', res);
        saveToken(res.data.data.token);
        Keyboard.dismiss();
        navigation.navigate('Profile');
      })
      .catch((err) => {
        console.log('error => ', err);
        Alert.alert('OOPS', 'check your email and password again', [
          {
            text: 'OK',
            onPress: () => {
              Keyboard.dismiss();
              console.log('alert close');
            },
          },
        ]);
      });
  };

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
        console.log('dismissed the keyboad');
      }}>
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Welcome back</Text>
          <View>
            <Text style={styles.subHeadText}>let's grow your business</Text>
          </View>
        </View>
        <View style={styles.form}>
          <TextInput
            value={email}
            placeholder="Email"
            onChangeText={(email) => setEmail(email)}
            style={styles.textInput}
          />
          <TextInput
            value={password}
            secureTextEntry={true}
            onChangeText={(password) => setPassword(password)}
            placeholder="Password"
            style={styles.textInput}
          />
          <View style={styles.buttonAlign}>
            <TouchableOpacity style={styles.theButton}>
              <Button
                title="LOGIN"
                color={colors.main}
                // onPress={() => navigation.navigate('Profile')}>
                onPress={() => onLoginPress()}></Button>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.subFootText}>
          <Text>don't have an account yet?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={{marginLeft: 10, fontWeight: 'bold'}}>REGISTER</Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    // backgroundColor: 'yellow',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
  },
  header: {
    marginTop: 40,
    paddingVertical: 10,
    paddingHorizontal: 30,
    // backgroundColor: 'blue',
    backgroundColor: '#ffffff',
    flexDirection: 'column',
  },
  subHeadText: {
    fontFamily: 'RobotoRegular',
    fontSize: 20,
    marginTop: 10,
  },
  subFootText: {
    flexDirection: 'row',
    fontFamily: 'ComfortaaBold',
    fontSize: 20,
    marginRight: 30,
    justifyContent: 'flex-end',
  },
  headerText: {
    fontFamily: 'RobotoBold',
    fontSize: 36,
  },
  form: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  formLabel: {
    paddingLeft: 20,
  },
  textInput: {
    margin: 15,
    padding: 5,
    height: 40,
    borderColor: 'black',
    borderWidth: 0.5,
    borderRadius: 2,
  },
  buttonAlign: {
    alignItems: 'stretch',
    marginHorizontal: 15,
  },
  theButton: {
    paddingVertical: 20,
  },
});
