import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  Modal,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import api from '../../../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {RNCamera} from 'react-native-camera';
const {width, height} = Dimensions.get('window');
export default function editProfile() {
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);
  // const[]
  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        // console.log('token => ', token);
        if (token !== null) return getProfile(token);
        // console.log(Profile);
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getToken();
  }, []);
  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            type={type}
            ref={(ref) => {
              camera = ref;
            }}></RNCamera>
        </View>
      </Modal>
    );
  };
  return (
    <View>
      <StatusBar backgroundColor="#00BCD4" barStyle="light-content" />
      <View style={styles.container}>
        {/* Header */}
        <View style={styles.header}>
          <Text style={styles.headerText}>Edit Profile</Text>
        </View>
        <View style={styles.blueContainer}>
          <TouchableOpacity
            style={styles.changePicture}
            onPress={() => setIsVisible(true)}>
            <Text style={styles.btnChangePicture}>Change picture</Text>
          </TouchableOpacity>
        </View>
      </View>
      {renderCamera()}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  header: {
    backgroundColor: 'black',
  },
  headerText: {
    marginVertical: width * 0.05,
    marginHorizontal: width * 0.05,
    fontSize: 24,
    color: 'white',
  },
});
