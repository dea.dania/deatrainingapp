import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Alert,
  Modal,
  StatusBar,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Button,
} from 'react-native';
// import styles from './style';
// import {Button} from '../components/Button';
import colors from '../style/colors';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import api from '../../../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
const {width, height} = Dimensions.get('window');
function EditProfile({navigation, route}) {
  let input = useRef(null);
  let camera = useRef(null);
  const [editable, setEditable] = useState(false);
  const [token, setToken] = useState('');
  const [name, setName] = useState(route.params.name);
  const [email, setEmail] = useState(route.params.email);
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);
  const [imageuri, setImageuri] = useState(`${api.home}${route.params.photo}`);

  // const [lemail, setLemail] = useState(null);
  // const [lpass, setPass] = useState(null);

  const toggleCamera = () => {
    setType(type == 'back' ? 'front' : 'back');
  };
  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        // const remail = await AsyncStorage.getItem('email');
        // const rpass = await AsyncStorage.getItem('pass');
        // setLemail(remail);
        // setPass(rpass);
        // console.log('token => ', token);
        if (token !== null) return setToken(token);
        // console.log(Profile);
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getToken();
  }, []);
  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.current.takePictureAsync(options);
      setPhoto(data);
      setIsVisible(false);
      console.log('editaccount=< data > ', data);
    }
  };
  const editData = () => {
    setEditable(!editable);
  };

  const onSavePress = async () => {
    const formData = new FormData();
    // const imgform = photo.uri !== null ? photo.uri : imageuri;
    formData.append('name', name);
    if (photo !== null) {
      formData.append('photo', {
        uri: photo.uri,
        name: 'photo.jpg',
        type: 'image/jpg',
      });
    }
    Axios.post(`${api.api}/profile/update-profile`, formData, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
        Accept: 'aplication/json',
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((res) => {
        console.log('res = >', res);
        console.log(res.request);
        // await AsyncStorage.removeItem('token');
        // await AsyncStorage.setItem('token', res.data.data.profile)
        // navigation.navigate('Profile');
        navigation.replace('Profile');
      })
      .catch((err) => {
        console.log('error=>', err);
        alert('Update Profile is Failed');
      });
  };
  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flexDirection: 'row', backgroundColor: colors.black}}>
          <TouchableOpacity onPress={() => toggleCamera()}>
            <View style={styles.btnToggleCamera}>
              <Ionicons
                name="camera-reverse-outline"
                size={50}
                color={colors.white}
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          <RNCamera style={{flex: 1}} type={type} ref={camera}></RNCamera>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              backgroundColor: colors.black,
            }}>
            <TouchableOpacity onPress={() => takePicture()}>
              <View style={styles.btnToggleCamera}>
                <Ionicons
                  name="camera-outline"
                  size={50}
                  color={colors.white}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  return (
    <>
      <StatusBar backgroundColor="#00BCD4" barStyle="light-content" />
      <View style={styles.container}>
        <View style={styles.header}>
          <Ionicons
            name="return-up-back"
            size={20}
            color={colors.white}
            onPress={() => navigation.navigate('Profile')}
          />
          <Text style={styles.headerText}>Edit Profile</Text>
        </View>
        <View style={styles.imgContainer}>
          <TouchableOpacity
            style={styles.changePicture}
            onPress={() => setIsVisible(true)}>
            <Image
              source={
                route.params.photo !== null && photo == null
                  ? {
                      uri: imageuri,
                      cache: 'reload',
                      headers: {Pragma: 'no-cache'},
                    }
                  : {
                      uri: photo.uri,
                      cache: 'reload',
                      headers: {Pragma: 'no-cache'},
                    }
              }
              style={styles.img}
            />
            <Icon name="edit-2" size={20} style={styles.btnEditImg} />
          </TouchableOpacity>
        </View>
        <View style={styles.dataContainer}>
          <View style={styles.dataItemContainer}>
            <Text style={styles.itemLabel}>Nama</Text>
            <View style={styles.btnEditText}>
              <TextInput
                value={name}
                style={styles.textInput}
                editable={editable}
                onChangeText={(value) => setName(value)}
              />
              <Icon name="edit-2" size={20} onPress={() => editData()} />
            </View>
          </View>
          <View style={styles.dataItemContainer}>
            <View style={styles.itemLabelContainer}>
              <Text style={styles.itemLabel}>Email</Text>
            </View>
            <TextInput
              value={email}
              style={styles.textInput}
              editable={false}
            />
          </View>
        </View>
        <View style={styles.btnContainer}>
          <Button
            title="SIMPAN"
            color={colors.main}
            onPress={() => onSavePress()}></Button>
        </View>
      </View>
      {renderCamera()}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'red',
  },
  header: {
    backgroundColor: '#00BCD4',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: width * 0.05,
  },
  headerText: {
    marginVertical: width * 0.05,
    marginHorizontal: width * 0.05,
    fontSize: 24,
    color: 'white',
  },
  camera: {
    backgroundColor: 'grey',
    justifyContent: 'center',
    borderRadius: 50,
  },
  changePicture: {
    flexDirection: 'row',
  },
  img: {
    width: height * 0.2,
    height: height * 0.2,
    borderWidth: 1,
    borderRadius: 100,
  },
  btnEditImg: {
    flexDirection: 'column-reverse',
    marginTop: height * 0.17,
    marginLeft: -height * 0.06,
    padding: 5,
    backgroundColor: colors.white,
    alignItems: 'center',
    // borderWidth: 1,
    borderRadius: 20,
  },
  btnCam: {
    backgroundColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    paddingVertical: 35,
  },
  imgContainer: {
    flex: 1,
    backgroundColor: '#dfe6e9',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnToggleCamera: {
    backgroundColor: colors.black,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
  },
  dataContainer: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  dataItemContainer: {
    flex: 1,
    backgroundColor: colors.white,
    justifyContent: 'space-around',
    paddingHorizontal: width * 0.06,
  },
  btnContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: width * 0.05,
  },
  itemLabel: {
    fontSize: height * 0.02,
  },
  textInput: {
    fontWeight: 'bold',
    fontSize: height * 0.03,
  },
  btnEditText: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
export default EditProfile;
