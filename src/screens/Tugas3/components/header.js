import React from 'react';
import {View, StyleSheet, Text, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

export default function Header() {
  return (
    <View style={styles.header}>
      <Text style={styles.headerText}>My Todo List</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#00BCD4',
    padding: height * 0.02,
  },
  headerText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: height * 0.03,
  },
});
