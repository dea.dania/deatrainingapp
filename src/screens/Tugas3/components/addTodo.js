import React, {useState} from 'react';
import {View, Dimensions, TextInput, Button, StyleSheet} from 'react-native';
const {width, height} = Dimensions.get('window');

export default function AddTodo({submitHandler}) {
  const [text, setText] = useState('');
  const changeHandler = (val) => {
    setText(val);
  };
  return (
    <View style={styles.input}>
      <TextInput
        style={styles.inputText}
        placeholder="new todo"
        onChangeText={changeHandler}
      />
      <Button title="+" onPress={() => submitHandler(text)} color="#00BCD4" />
    </View>
  );
}
const styles = StyleSheet.create({
  input: {
    flexDirection: 'row',
    alignItems: 'stretch',
    margin: height * 0.02,
  },
  inputText: {
    borderBottomWidth: 1,
    borderBottomColor: '#d2dae2',
    paddingLeft: height * 0.02,
    width: width * 0.86,
  },
});
