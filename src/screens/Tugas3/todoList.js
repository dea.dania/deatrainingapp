import React, {useState} from 'react';
import {
  View,
  Dimensions,
  Text,
  StyleSheet,
  FlatList,
  StatusBar,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import Header from './components/header';
import TodoItem from './components/todoItem';
import AddTodo from './components/addTodo';
const {width, height} = Dimensions.get('window');

export default function todoList() {
  const [todos, setTodos] = useState([
    {text: 'read some books', key: '1', date: '01/02/2020'},
    {text: 'play basketball', key: '2', date: '08/03/2020'},
    {text: 'zumba dance', key: '3', date: '05/04/2020'},
  ]);
  const pressHandler = (key) => {
    var theTodo = todos.filter((todo) => todo.key === key)[0].text;
    Alert.alert('Verify to delete todo', theTodo, [
      {
        text: 'yes',
        onPress: () => {
          setTodos((prevTodos) => {
            return prevTodos.filter((todo) => todo.key != key);
          });
        },
      },
      {
        text: 'no',
        onPress: () => {
          console.log('alert close');
        },
      },
    ]);
  };
  const submitHandler = (t) => {
    const day = new Date().getDate();
    const month = new Date().getMonth();
    const year = new Date().getFullYear();

    const today = `${day}/${month}/${year}`;
    if (t.length > 3) {
      setTodos((prevTodos) => {
        return [
          {key: Math.random().toString(), text: t, date: today},
          ...prevTodos,
        ];
      });
    } else {
      Alert.alert('Caution', 'The input must be more than 3 characters', [
        {
          text: 'understood',
          onPress: () => {
            console.log('alert close');
          },
        },
      ]);
    }
  };
  return (
    <>
      <StatusBar backgroundColor="#00BCD4" barStyle="light-content" />
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
          console.log('dismissed the keyboad');
        }}>
        <View style={styles.container}>
          <Header />
          <AddTodo submitHandler={submitHandler} />
          {/* Flatlist */}
          <View style={styles.list}>
            <FlatList
              data={todos}
              renderItem={({item}) => (
                <TodoItem item={item} pressHandler={pressHandler} />
              )}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: height * 0.00005,
  },
});
