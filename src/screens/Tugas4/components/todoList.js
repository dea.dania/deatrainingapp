import React, {useContext} from 'react';
import {
  View,
  Dimensions,
  Text,
  StyleSheet,
  FlatList,
  StatusBar,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import Header from './header';
import TodoItem from './todoItem';
import AddTodo from './addTodo';
import {RootContext} from '../index';
const {width, height} = Dimensions.get('window');

export default function todoList() {
  const state = useContext(RootContext);
  return (
    <>
      <StatusBar backgroundColor="#00BCD4" barStyle="light-content" />
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
          console.log('dismissed the keyboad');
        }}>
        <View style={styles.container}>
          <Header />
          <AddTodo />
          {/* Flatlist */}
          <View style={styles.list}>
            <FlatList
              data={state.todos}
              renderItem={({item}) => (
                <TodoItem item={item} pressHandler={state.pressHandler} />
              )}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: height * 0.00005,
  },
});
