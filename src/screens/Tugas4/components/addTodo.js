import React, {useContext} from 'react';
import {View, Dimensions, TextInput, Button, StyleSheet} from 'react-native';
import {RootContext} from '../index';

const {width, height} = Dimensions.get('window');

export default function AddTodo() {
  const state = useContext(RootContext);

  return (
    <View style={styles.input}>
      <TextInput
        style={styles.inputText}
        value={state.text}
        placeholder="new todo"
        onChangeText={state.changeHandler}
      />
      <Button
        title="+"
        onPress={() => state.submitHandler(state.text)}
        color="#00BCD4"
      />
    </View>
  );
}
const styles = StyleSheet.create({
  input: {
    flexDirection: 'row',
    alignItems: 'stretch',
    margin: height * 0.02,
  },
  inputText: {
    borderBottomWidth: 1,
    borderBottomColor: '#d2dae2',
    paddingLeft: height * 0.02,
    width: width * 0.86,
  },
});
