import React, {useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Delete from '../../../assets/icons/trash.svg';
import {RootContext} from '../index';

const {width, height} = Dimensions.get('window');

export default function TodoItem({item}) {
  const state = useContext(RootContext);

  return (
    <View style={styles.item}>
      <Text style={styles.itemDate}>{item.date}</Text>
      <Text style={styles.itemText}>{item.text}</Text>
      <TouchableOpacity
        onPress={() => state.pressHandler(item.key)}
        style={styles.button}>
        <Delete height={50} width={50} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  item: {
    marginHorizontal: width * 0.04,
    marginVertical: height * 0.005,
    padding: height * 0.02,
    borderRadius: 10,
    borderWidth: 1,
    borderStyle: 'dashed',
    borderColor: 'grey',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  itemText: {
    width: width * 0.7,
  },
  itemDate: {
    width: width * 0.7,
    fontWeight: 'bold',
  },
  button: {
    alignItems: 'flex-end',
  },
});
