import React, {useState, createContext} from 'react';
import {Alert} from 'react-native';
import TodoList from './components/todoList';

export const RootContext = createContext();
const Context = () => {
  const [todos, setTodos] = useState([
    {text: 'read some books', key: '1', date: '01/02/2020'},
    {text: 'play basketball', key: '2', date: '08/03/2020'},
    {text: 'zumba dance', key: '3', date: '05/04/2020'},
  ]);
  const [text, setText] = useState('');
  const changeHandler = (val) => {
    setText(val);
  };
  const pressHandler = (key) => {
    var theTodo = todos.filter((todo) => todo.key === key)[0].text;
    Alert.alert('Verify to delete todo', theTodo, [
      {
        text: 'yes',
        onPress: () => {
          setTodos((prevTodos) => {
            return prevTodos.filter((todo) => todo.key != key);
          });
        },
      },
      {
        text: 'no',
        onPress: () => {
          console.log('alert close');
        },
      },
    ]);
  };
  const submitHandler = (t) => {
    const day = new Date().getDate();
    const month = new Date().getMonth();
    const year = new Date().getFullYear();

    const today = `${day}/${month}/${year}`;
    if (text.length > 3) {
      setTodos((prevTodos) => {
        return [
          {key: Math.random().toString(), text: t, date: today},
          ...prevTodos,
        ];
      });
      setText('');
    } else {
      Alert.alert('Caution', 'The input must be more than 3 characters', [
        {
          text: 'understood',
          onPress: () => {
            console.log('alert close');
          },
        },
      ]);
    }
  };
  return (
    <RootContext.Provider
      value={{
        text,
        todos,
        changeHandler,
        pressHandler,
        submitHandler,
      }}>
      <TodoList />
    </RootContext.Provider>
  );
};

export default Context;
